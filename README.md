<!-- Título -->
<div align="center">
    <h1><b>Apresentação de projeto de engenharia de software</b></h1>
</div>

<!-- Insígnias-->
<div align="center">
    ![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-informational?style=for-the-badge&logo=appveyor)
</div>

<!-- Seções -->
## Propósito do repositório

Este repositório tem como objetivo guardar os documentos de organização do
processo de produção e apresentação do software desenvolvido pelo grupo dos
alunos Andreas R. Spyridakis, Lucas B. de Araújo, Marina R. Kehagias, Lucas O.
M. da Rocha, Éric de B. Moreira e Davi D. Torres da turma do 1o semestre de 2021
do UniCEUB da disciplina de engenharia de software.

## Sobre o modelo latex da apresentação

Os slides de apresentação (localizados dentro da pasta src como arquivo `.pdf`)
utilizam um modelo que foi inspirado no modelo (template) de apresentação de
[Maurício Moreira Neto](https://www.overleaf.com/latex/templates/template-beamer-ufc/rvqwnmszpsvf).
