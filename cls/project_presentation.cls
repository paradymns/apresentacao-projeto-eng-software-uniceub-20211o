% Title: Project presentation class file
%
% Author: paradymns
%
% Date: 2021/03/01
%
% Time: 12:01
%
% Description: this file creates a custom beamer class for presentations of
% university projects.

%
% Start of `project_presentation.cls' file
%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{../cls/project_presentation}[01/03/2021 v1.0 presentation template beamer]

\PassOptionsToPackage{svnames}{xcolor}
\LoadClass[compress, 10pt]{beamer}

% Use a background image in all frames:
% \usebackgroundtemplate{\includegraphics[width=1.7\paperwidth]{../images/logoUniCEUBbackground.png}}
% \logo{\includegraphics[scale=0.1]{../images/emblemUniCEUB.pdf}\hspace{9.7cm} \vspace{-0.2cm}}

% Packages to be used:
\RequirePackage[utf8]{inputenc}
\RequirePackage[brazil]{babel}
\RequirePackage{graphicx}
\RequirePackage{hyperref}
\RequirePackage{microtype}
\RequirePackage[T1]{fontenc}
\RequirePackage{helvet}
\RequirePackage{pdfpages}
\RequirePackage{tcolorbox}
\RequirePackage{array}
\RequirePackage{colortbl}
\RequirePackage{graphicx}
\RequirePackage{fancybox}
\RequirePackage[footnotesize,hang]{caption}
% Option management:
\RequirePackage{beamerbaseoptions}

% Define colors:
\definecolor{purple_theme}{RGB}{67, 5, 78}
\definecolor{yellow}{RGB}{206, 177, 68}
\definecolor{black}{RGB}{0, 0, 0}
\definecolor{white}{RGB}{255, 255, 255}
\definecolor{red}{RGB}{215, 0, 0}
\definecolor{light_purple_uniceub}{RGB}{192, 0, 136}

% Style of the blocks uniceub:
\beamerboxesdeclarecolorscheme{blockUniCEUB}{purple_theme}{white}
\beamer@autopdfinfotrue
\beamer@notesnormaltrue

\usecolortheme[named=light_purple_uniceub]{structure}

\useinnertheme{rectangles}
\useoutertheme[subsection=false]{miniframes}
\setbeamertemplate{navigation symbols}{}

\setbeamercolor{section in head/foot}{bg=purple_theme, fg=white}
\setbeamercolor{subsection in head/foot}{bg=yellow, fg=white}
\setbeamercolor{institute in head/foot}{bg=purple_theme, fg=white}
\setbeamerfont{block title}{size={}}

% Bottom fields:
\setbeamercolor{author in head/foot}{bg=purple_theme, fg=white}
\setbeamercolor{title in head/foot}{bg=yellow, fg=white}
\setbeamercolor{date in head/foot}{bg=purple_theme, fg=white}

% Footer style:
\defbeamertemplate*{footline}{infolines theme}{
  \leavevmode%
  \hbox{%
    
    %\begin{beamercolorbox}[wd=.333333\paperwidth,ht=2.25ex,dp=1ex,center]{author in head/foot}
      %\usebeamerfont{author in head/foot}\insertshortauthor~~\insertshortinstitute
    %\end{beamercolorbox}

    \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}
      \insertshorttitle
      \usebeamerfont{title in head/foot}
    \end{beamercolorbox}

    \begin{beamercolorbox}[wd=.5\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}
      \usebeamerfont{date in head/foot}\insertshortdate{}\hspace*{2em}
      \insertframenumber{} / \inserttotalframenumber\hspace*{2ex}
    \end{beamercolorbox}
  }
  
  \vskip0pt
}
\mode
<all>

% Command of the example:
\renewcommand{\example}[1]{\textcolor{purple_theme}{\textbf{#1}}}
% Command of the emphases:
\renewcommand{\emph}[1]{\textcolor{red}{\textbf{#1}}}

% Create the blocks of the beamer template

%% Draw the blocks:
\setbeamertemplate{blocks}[rounded][shadow=true]
% \setbeamercolor{separation line}{use=structure,bg=structure.fg!50!bg} % Color on the top.

%% Block:
\setbeamercolor*{block title}{fg=white, bg=purple_theme}
\setbeamercolor*{block body}{fg=black, bg=white}
%% Alert block:
\setbeamercolor*{block title alerted}{fg=white,bg=red}
\setbeamercolor*{block body alerted}{fg=black,bg=white}
%% Example block:
\setbeamercolor*{block title example}{fg=white,bg=light_purple_uniceub}
\setbeamercolor*{block body example}{fg=black,bg=white}

%% Creating the environment of the block:
\usepackage{etoolbox}
\AtBeginEnvironment{exampleblock}{
  \setbeamercolor{itemize item}{fg=light_purple_uniceub!70}
}
\AtBeginEnvironment{alertblock}{
  \setbeamercolor{itemize item}{fg=red!70}
}
\AtBeginEnvironment{block}{
  \setbeamercolor{itemize item}{fg=purple_theme!70}
}

% Determine some colors for the text:
\setbeamercolor*{normal text}{fg=black,bg=white}
\setbeamercolor*{example text}{fg=yellow}
\setbeamercolor*{alerted text}{fg=red}
\setbeamercolor{itemize item}{fg=purple_theme!70}
\setbeamercolor{enumerate item}{fg=black!70}
\setbeamercolor{success}{fg=light_purple_uniceub}

% Create the alert box:
\newcommand{\alertbox}[1]{
  \begin{flushleft}
    \fcolorbox{red}{white}{
      \begin{minipage}{0.9\textwidth}
        #1
      \end{minipage}
    }
  \end{flushleft}
}

% Create the simple box:
\newcommand{\simplebox}[1]{
  \begin{flushleft}
    \fcolorbox{purple_theme}{white}{
      \begin{minipage}{0.9\textwidth}
        #1
      \end{minipage}
    }
  \end{flushleft}
}

% Create the success box:
\newcommand{\successbox}[1]{
  \begin{flushleft}
    \fcolorbox{light_purple_uniceub}{white}{
      \begin{minipage}{0.9\textwidth}
        #1
      \end{minipage}
    }
  \end{flushleft}
}

% Create the source of the image:
\newcommand{\source}[1]{
  \caption*{\textcolor{purple_theme}{Fonte:} {#1}}
}

% Create a command for UniCEUB:
\newcommand{\uniceub}{
  \bfseries
  \normalsize{Centro Universitário de Brasília}
}

% Department command:
\newcommand{\department}[1]{
  \vspace*{0.2cm}
  \bfseries
  \normalsize{#1}
}

% Create command for e-mail:
\newcommand{\email}[1]{
  \texttt{
    \href{mailto:#1}{#1}
  }
}

%
% End of `project_presentation.cls' file
%
